import random


listaPalo = ['bastos','espadas','copas','oros']
listaNumeros = ['A','1','2','3','4','5','6','7','S','C','R']
listaJugadores = {}

baraja = []
barajaNueva = []

#CREAR BARAJA con la iteración de dos listas. La lista donde están los diferentes valores numéricos de las cartas y la lista donde están los distintos palos.

for basto in listaPalo:
    for numero in listaNumeros:        
        carta = '{}{}'.format(numero,basto[0])
        baraja.append(carta)




def validar(num):
    
    if isinstance(num,int):
        return True
    
    else:
        return False

    return num
    
#BARAJEAR // si quiero que se baraje una vez se han repartido cartas, tendría que eliminar de forma aleatoria las cartas repartidas de la baraja.
 #Las cartas repartidas también pueden ser igual a 0. Es decir, que esté la baraja entera.

def barajear(cartasRepartidas):
     
    if validar(cartasRepartidas) == True:
        
        n = cartasRepartidas
        
        if n > 0  and validar(n) == True:
        
            numCartas = len(baraja) - n
                
            while len(barajaNueva) <= numCartas:
                n = random.randint(0,len(baraja)-1-n)
                newValor = baraja[n]
                barajaNueva.append(newValor)
                baraja.remove(newValor)
        
           

        else:
            for i in baraja:
                barajaNueva.append(i)
    
    else:
        print('Hay un error.')
    
    return barajaNueva


#REAPARTIR EN FUNCION DE CARTAS EN MANO Y JUGADORES

def repartir(mano, jugadores):
    
    if validar(mano) == True and validar(jugadores) == True:  
        
        value = 0
        n = 0
        valorInicial = mano
            
        if mano*jugadores <= len(barajaNueva):
            while value <= jugadores :
                indice = str(value)
                listaJugadores['Jugador' + indice] = barajaNueva[n:valorInicial+n]

                n += mano
                value += 1
                
            for i in range (0,len(listaJugadores)-1):
                indice = str(i)
                print('Jugador {}:{}'.format(i, listaJugadores['Jugador'+indice]))
                
        else:
            print('No hay suficientes cartas para todos')

    else:
        print('Hay un error')