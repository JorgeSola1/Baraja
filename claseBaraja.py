import random

class Baraja():
    
    def __init__(self):
        
        self.listaPalo = ['bastos','espadas','copas','oros']
        self.listaNumeros = ['A','1','2','3','4','5','6','7','S','C','R']
        self.listaJugadores = {}
        self.baraja = []
        self.barajaNueva = []
        __cartasRepartidas = 0
        __mano = 0
        __jugadores = 0
        
    def crearBaraja(self):
    #CREAR BARAJA        
        for basto in self.listaPalo:
            for numero in self.listaNumeros:        
                carta = '{}{}'.format(numero,basto[0])
                self.baraja.append(carta)

        

    
    def validar(self,num):
    
        if isinstance(num,int):
            return True
        
        else:
            return False

        return num
    
    #BARAJEAR // si quiero que se baraje una vez se han repartido cartas, tendría que eliminar de forma aleatoria
    # un numero 'n' de cartas

    

    def barajear(self, cartasRepartidas):
         
    
        n = cartasRepartidas
        
        if n > 0  and self.validar(n) == True:
        
            numCartas = len(self.baraja) - n
                
            while len(self.barajaNueva) <= numCartas:
                n = random.randint(0,len(self.baraja)-1-n)
                newValor = self.baraja[n]
                self.barajaNueva.append(newValor)
                self.baraja.remove(newValor)
        
       

        else:
            for i in self.baraja:
                self.barajaNueva.append(i)
          
        return self.barajaNueva
#REAPARTIR EN FUNCION DE CARTAS EN MANO Y JUGADORES

    def repartir(self, mano, jugadores):
        
            
        value = 0
        n = 0
        valorInicial = mano
            
        if mano*jugadores <= len(self.barajaNueva):
            while value <= jugadores :
                indice = str(value)
                self.listaJugadores['Jugador' + indice] = self.barajaNueva[n:valorInicial+n]

                n += mano
                value += 1
                
            for i in range (0,len(self.listaJugadores)-1):
                indice = str(i)
                print('Jugador {}:{}'.format(i, self.listaJugadores['Jugador'+indice]))
                
        else:
            print('No hay suficientes cartas para todos')
            
    
   
class mainApp():
    
    def __init__(self):
    
        self.baraja = Baraja()
        self.baraja.crearBaraja()
        self.baraja.barajear(0)
        self.baraja.repartir(5,6)
        
    
        
        

if __name__ == '__main__':
    
    juego = mainApp()
    